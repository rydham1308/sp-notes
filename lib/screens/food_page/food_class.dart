class FoodModelClass {
  final String image;
  final String name;
  final String kcal;
  final String time;
  final int day;
  final String type;

  FoodModelClass({
    required this.image,
    required this.name,
    required this.kcal,
    required this.time,
    required this.day,
    required this.type,
  });
}
