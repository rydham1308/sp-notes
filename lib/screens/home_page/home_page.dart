import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/screens/home_page/bottom_sheet.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  String name = 'No Data';
  List<String> getNameList = [];
  int delIndex = 0;
  int editIndex = 0;
  bool isEdit = false;

  @override
  void initState() {
    super.initState();
    getName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(
          'SP Notes',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.blue,
        shadowColor: Colors.blue,
        elevation: 15,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/dashboard');
            },
            icon: const Icon(CupertinoIcons.person, color: Colors.white),
          ),
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, '/food');
            },
            icon: const Icon(CupertinoIcons.circle_grid_hex, color: Colors.white),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
            child: getNameList.isNotEmpty
                ? ListView.builder(
                    itemCount: getNameList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () async {
                          await getBottomModelSheet(getNameList[index],
                              true); // Sends the stored data to bottom sheet
                          isEdit = true;
                          editIndex = index;
                          // final data = await Navigator.pushNamed(
                          //     context, '/addScreen',
                          //     arguments: getNameList[
                          //         index]); // Sends the stored data to bottom sheet
                          // isEdit = true;
                          // editIndex = index;
                          // if (data is bool) {
                          //   editName();
                          // }
                        },
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Text(
                                    getNameList[index],
                                    overflow: TextOverflow.visible,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: GestureDetector(
                                    onTap: () {
                                      delIndex = index;
                                      setState(() {
                                        delName();
                                      });
                                    },
                                    child: const Icon(
                                      Icons.delete_forever,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  )
                : const Center(
                    child: Text(
                      'No Data',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  )),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // This is for another Screen
          // final data = await Navigator.pushNamed(context, '/addScreen');
          // if (data is bool) {
          //   getName();
          // }
          await getBottomModelSheet(null, false);
        },

        backgroundColor: Colors.blue,
        tooltip: 'Add',
        splashColor: Colors.blue,
        elevation: 25,
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }

  //#region -- Methods
  Future<void> getBottomModelSheet(String? storedValue, bool isEditBool) async {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.blue,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), topLeft: Radius.circular(30)),
        ),
        context: context,
        builder: (context) {
          return Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: AddSheet(storedValue));
        }).then((value) {
      if (value??false) {
        isEditBool ? editName() : getName();
      }
    });
  }

  void editName() async {
    var sharedPref = await SharedPreferences.getInstance();
    var getName = sharedPref.getString(AddSheetState.nameKey);
    setState(() {
      getNameList[editIndex] = getName ??
          ''; // First Updates the data in getName list and then sets to key's value
      sharedPref.setStringList(AddSheetState.nameListKey, getNameList);
    });
  }

  void delName() async {
    var sharedPref = await SharedPreferences.getInstance();
    setState(() {
      getNameList.removeAt(
          delIndex); // First removes the data in getName list and then sets to key's value
      sharedPref.setStringList(AddSheetState.nameListKey, getNameList);
    });
  }

  void getName() async {
    var sharedPref = await SharedPreferences.getInstance();
    List<String>? getList = sharedPref.getStringList(AddSheetState.nameListKey);
    setState(() {
      getNameList.addAll(List.from(getList??[]));
      sharedPref.setStringList(AddSheetState.nameListKey, getNameList);
    });
  }
//endregion
}
