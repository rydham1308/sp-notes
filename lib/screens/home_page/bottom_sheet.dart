import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddSheet extends StatefulWidget {
  const AddSheet(this.storedValue, {super.key});
  final String? storedValue;
  @override
  State<AddSheet> createState() => AddSheetState();
}

class AddSheetState extends State<AddSheet> {
  GlobalKey<FormState> formAddKey = GlobalKey<FormState>();
  TextEditingController name = TextEditingController();
  static const String nameKey = 'nameKey';
  static const String nameListKey = 'nameListKey';
  bool isEditBtn = false;

  @override
  void didChangeDependencies() {
    final getOldName = widget.storedValue;
    if (getOldName is String) {
      name.text = getOldName;
      isEditBtn = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(left: 23, top: 20),
            alignment: Alignment.centerLeft,
            child: Image.asset('assets/images/wired.gif', scale: 4,color: Colors.black),
          ),
          Container(
            margin: const EdgeInsets.only(left: 23, bottom: 15, top: 20),
            alignment: Alignment.centerLeft,
            child:!isEditBtn
                ? const Text(
              "Add Note",
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
            )
                : const Text(
              "Update Note",
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
            ),
          ),
          Form(
            key: formAddKey,
            child: Column(
              children: [
                Container(
                  margin:
                  const EdgeInsets.only(right: 23, left: 23, bottom: 20),
                  child: TextField(
                    controller: name,
                    cursorColor: Colors.black,
                    style: const TextStyle(color: Colors.black),
                    decoration: const InputDecoration(
                      labelText: 'Note',
                      labelStyle: TextStyle(
                        color: Colors.black,
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 23, bottom: 15),
            alignment: Alignment.centerLeft,
            child: ElevatedButton(
              onPressed: () async {
                var sharedPref = await SharedPreferences.getInstance();
                if (isEditBtn) {
                  sharedPref.setString(nameKey, name.text);
                  isEditBtn = false;
                } else {
                  sharedPref.setStringList(nameListKey, [name.text]);
                }
                Future.delayed(Duration.zero)
                    .then((value) => Navigator.pop(context, true));
              },
              style: ElevatedButton.styleFrom(
                shadowColor: Colors.transparent,
                backgroundColor: Colors.black,
                elevation: 0,
                shape: const StadiumBorder(),
                minimumSize: const Size(150, 40),
              ),
              child: !isEditBtn
                  ? const Text(
                "Add",
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 15,
                  color: Colors.blue,
                ),
              )
                  : const Text(
                "Update",
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 15,
                  color: Colors.blue,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
