//#region -- Strings

// Product 1

import 'dart:ui';

const String image1 =
    'https://s3-alpha-sig.figma.com/img/d1f5/9d16/5db67dc122ccd50c62d70790b84282c9?Expires=1708905600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=TyKdzTJX3JXhVnGTuQ049JETfUSBgKc0djdcF0DCJzr9YwvA2kDkXg5FnHT0JUtlc5CuHeHgNh17~HQjuerEz9Kxh6f63Cts9X0ZhXoPBm9NDsXg4bOLdEEN2sWDZYlI9cdS0pUuNydf0P3ERIu3xGvjIRM~mwjx3NYb79GRtHhRgzYRlOG1xCWlkkuH43Y8BWn54JZAhH1V2Q83ldPFPDc4MRoxyAf5YDUjWyp2Y7yNRbo6-Hp~HBTDHEWGrPc5-wLMsxzyy~V04j2Z8oDrVzrJP0zZ5SKGTc4FCLVpjEn9KjgJ-sQ6ooCqreK66T~MCYjnKVfVa7GiyYeDzQ02jw__';
const String name1 = 'Avocado Toast';
const String kcal1 = '1000';
const String time1 = '10';
const int day1 = 1;
const String type1 = 'Breakfast';

// Product 2

const String image2 =
    'https://s3-alpha-sig.figma.com/img/f57c/d5e1/7a9c403920437303291688489d9a927b?Expires=1708905600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=kyMmTmWmVu14fsf7qw~eovU9eyiArY~F1RROXB6DqEMQOUnRLRED0vX3yupHx1yVCjTUtOTdlcaqzE4qhioAlz6VHQeWmdvdGSDo4udIjp~c~J-7R~TdtqpQbf0rnyUVNth7st2YVXPnSf5WgMffXwwQ3YEUNizuHr~lby8E1wPw0-imhvJu7S~BZ8u6hvQcmZseD3jyRvxlBEp0F~cLH2VQls52~-4~1ObukvkXwfCS2EFJUGT5v63HE3ue~ROpsvsGw7zdWSzebM9AUChcAVFgvGMBscAaOF00kS3~k8~d2ZcNvp5Pv9acno3NjJEgYdKXlRn0EAWvgActFRi5DQ__';
const String name2 = 'Fried Rice';
const String kcal2 = '2190';
const String time2 = '20';
const int day2 = 1;
const String type2 = 'Lunch';

// Product 3

const String image3 =
    'https://s3-alpha-sig.figma.com/img/ae55/b2b1/0d23c90a41ca0bc07e1661fac21604d2?Expires=1708905600&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=OucDEy0liI2fDMGgvHMkgFuxnjL1nkaI5HkbW43rTIP9o4c8UNrYUgUdZ7w9ao3Z-sFJpzL43SXpFzckmTHwmBTtnzqEVMAdtwU-W0Zy2CSzKYSYSAV~h4io~VuhzSE-2~K-D9hdnGm2IYAxjjnU1DXqO0kOYf1NMA6o06EuB4VF7GxM7VLLzu~VVyE00paDhKXlHxoUGP~2XQcm4UOMsAmrWlCBa4r6TE9-~ygXcuwHWJpE7So5oxGLDnIKxKI5LNuyZ2nHCoQ~jyOcmA2UfcBcO31t4JPu6ymG9BtmWSt8QcwRm0gmPF2PzKPqoNBEVnKqCsvq24YCe5kxz3lH3A__';
const String name3 = 'Avocado Salad';
const String kcal3 = '560';
const String time3 = '10';
const int day3 = 1;
const String type3 = 'Dinner';

//endregion

//#region -- Colors

abstract class AppColors {
  static const colorYellow = Color(0xFFF2C94C);
  static const colorGrey = Color(0xFF242424);
}

//endregion
