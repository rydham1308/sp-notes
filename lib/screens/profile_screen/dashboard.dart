import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/Screens/Login/login.dart';
import 'package:test_app/Screens/Login/splash_screen.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => DashboardState();
}

class DashboardState extends State<Dashboard> {
  String email = '';
  String pass = '';

  @override
  void initState() {
    super.initState();

    getEmailPass();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Dashboard'),
        backgroundColor: Colors.blue,
        shadowColor: Colors.blue,
      ),
      body: Center(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [

            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Image.asset('assets/images/wired.gif', scale: 2,color: Colors.white),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Text(
                    "Welcome,",
                    style: TextStyle(fontSize: 40),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Text(
                    'Email | $email',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Text(
                    'Password | $pass',
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
                SizedBox(height: 20,),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(10),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            elevation: 20,
            backgroundColor: Colors.blue,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0)),
            minimumSize: const Size(double.maxFinite, 48),
          ),
          onPressed: () async {
            var sharedPref = await SharedPreferences.getInstance();
            sharedPref.setString(LoginState.EMAILKEY, '');
            sharedPref.setString(LoginState.PASSKEY, '');
            sharedPref.setBool(SplashScreenState.KEYLOGIN, false);

            Future.delayed(Duration.zero).then((value) =>Navigator.pushReplacementNamed(context, '/login'));
          },
          child: const Text(
            'Log Out',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  void getEmailPass() async {
    var sharedPref = await SharedPreferences.getInstance();
    var getEmail = sharedPref.getString(LoginState.EMAILKEY);
    var getPass = sharedPref.getString(LoginState.PASSKEY);

    setState(() {
      email = getEmail ?? '';
      pass = getPass ?? '';
    });
  }
}
