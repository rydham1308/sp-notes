import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_app/Screens/Login/login.dart';
import 'package:test_app/Screens/Login/register_screen.dart';
import 'package:test_app/Screens/Login/splash_screen.dart';
import 'package:test_app/screens/food_page/fodd_add.dart';
import 'package:test_app/screens/food_page/food_screen.dart';
import 'package:test_app/screens/profile_screen/dashboard.dart';
import 'package:test_app/screens/home_page/home_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ],
  );
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});



  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue,brightness: Brightness.dark),
        useMaterial3: true,
        fontFamily: 'MundialRegular',
      ),

      initialRoute: '/',
      routes: {
        '/': (context) => const SplashScreen(),
        '/login': (context) => const Login(),
        '/register': (context) => const RegisterScreen(),
        '/home': (context) => const HomePage(),
        '/dashboard': (context) => const Dashboard(),
        '/food': (context) => const FoodScreen(),
        '/foodAdd': (context) => const FoodAdd(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
